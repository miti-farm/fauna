# Fauna

> everything that's alive

Data layer for cloud services

- postgres

## Prerequisites

An [internet accessible](https://gitlab.com/deposition.cloud/infra/cluster/aurora) MicroK8s cluster with.

## Develop and Deploy

``` bash
pulumi config set kubernetes:context omega # or
pulumi config set kubernetes:context live
```

Pre-requisites for Helm.

``` bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### Pulumi Stack Config

``` bash
pulumi config set --plaintext tld cuciureanu.com # replace with your domain
pulumi config set --secret adminEmail noreply@cuciureanu.com
```

### PostgreSQL

``` bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

### Deploy

``` bash
pulumi up -f -y
```

We should now be able to securely access: [nginx.cuciureanu.com](https://nginx.cuciureanu.com)

Yey! :fireworks:

## Troubleshooting

Many things could go wrong. :smile:
